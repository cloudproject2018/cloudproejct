package com.example.hazem.tryfirebase.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.hazem.tryfirebase.MainActivity;
import com.example.hazem.tryfirebase.R;
import com.example.hazem.tryfirebase.data.SharedPreferenceHelper;
import com.example.hazem.tryfirebase.data.StaticConfig;
import com.example.hazem.tryfirebase.model.Consersation;
import com.example.hazem.tryfirebase.model.ImageMassage;
import com.example.hazem.tryfirebase.model.Message;
import com.example.hazem.tryfirebase.util.ImageUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

import de.hdodenhof.circleimageview.CircleImageView;


public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recyclerChat;
    public static final int VIEW_TYPE_USER_MESSAGE = 0;
    public static final int VIEW_TYPE_FRIEND_MESSAGE = 1;
    private ListMessageAdapter adapter;
    private String roomId;
    private ArrayList<CharSequence> idFriend;
    private Consersation consersation;
    private ImageButton btnSend;
    private ImageButton upload_images;
    private TextView isTyping;
    private TextView isSeen;
    private EditText editWriteMessage;
    private LinearLayoutManager linearLayoutManager;
    public static HashMap<String, Bitmap> bitmapAvataFriend;
    public Bitmap bitmapAvataUser;
    private String KeyOfMassage;
    private HashMap update_massage = new HashMap();
    private HashMap Config = new HashMap();
    private String idSender;
    private boolean isTyping_reciver = false;
    private boolean isTyping_sender = false;
    private boolean seen_reciver =false;
    private String Backgound_Color = "";
    private RelativeLayout chat_layout;
    final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private Context context;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private int massage_length =0;
    StorageReference storageRef = FirebaseStorage.getInstance().getReference();
    private static final int PICK_IMAGE = 1996;
    private ArrayList<Integer>  waiting_Images =new ArrayList<Integer>();
    private boolean status_my_friend = false;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);


        Intent intentData = getIntent();
        context = this;
        idFriend = intentData.getCharSequenceArrayListExtra(StaticConfig.INTENT_KEY_CHAT_ID);
        roomId = intentData.getStringExtra(StaticConfig.INTENT_KEY_CHAT_ROOM_ID);
        String nameFriend = intentData.getStringExtra(StaticConfig.INTENT_KEY_CHAT_FRIEND);

        consersation = new Consersation();
        btnSend = (ImageButton) findViewById(R.id.btnSend);
        upload_images =(ImageButton)findViewById(R.id.upload_image);
        isTyping = (TextView) findViewById(R.id.isTyping);
        isSeen = (TextView) findViewById(R.id.isSeen);
        chat_layout = (RelativeLayout)findViewById(R.id.chat_layout);
        // rl.setBackgroundColor(Color.RED);
        upload_images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });
          Config.put("isTyping_reciver",isTyping_reciver);
          Config.put("isTyping_sender",isTyping_sender);
          Config.put("Backgound_Color",Backgound_Color);
          Config.put("idSender",idSender);
          Config.put("seen_reciver",seen_reciver);



        btnSend.setOnClickListener(this);
        databaseReference.child("message").child(roomId).child("ConfigRoom").updateChildren(Config);

         bitmapAvataUser = SharedPreferenceHelper.getInstance(this).getUserInfo().myImage;


        editWriteMessage = (EditText) findViewById(R.id.editWriteMessage);
        editWriteMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) {
                    //Set the value of typing field to true.
                    if (idSender != null) {
                        if (idSender.equals(StaticConfig.UID)) {
                            Config.put("isTyping_sender", true);
                            databaseReference.child("message").child(roomId).child("ConfigRoom").updateChildren(Config);
                        } else {
                            Config.put("isTyping_reciver", true);
                            databaseReference.child("message").child(roomId).child("ConfigRoom").updateChildren(Config);
                        }
                    }

                } else {
                    if (idSender != null){
                        if (idSender.equals(StaticConfig.UID)) {
                            Config.put("isTyping_sender", false);
                            databaseReference.child("message").child(roomId).child("ConfigRoom").updateChildren(Config);
                        } else {
                            Config.put("isTyping_reciver", false);
                            databaseReference.child("message").child(roomId).child("ConfigRoom").updateChildren(Config);
                        }
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


       databaseReference.child("message").child(roomId).child("ConfigRoom")
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    HashMap data = (HashMap) dataSnapshot.getValue();
                    isTyping_reciver = (Boolean) data.get("isTyping_reciver");
                    isTyping_sender = (Boolean) data.get("isTyping_sender");
                    Backgound_Color = (String) data.get("Backgound_Color");
                    idSender = (String)data.get("idSender");
                    seen_reciver = (Boolean) data.get("seen_reciver");
                    Change_Background();
                    if(idSender != null){
                        if(idSender.equals(StaticConfig.UID)){
                            isSeen.setVisibility(View.VISIBLE);
                        }
                    if (idSender.equals(StaticConfig.UID)) {
                        if (isTyping_reciver) {
                            isTyping.setVisibility(View.VISIBLE);
                        } else {
                            isTyping.setVisibility(View.INVISIBLE);
                        }

                        if(seen_reciver){
                            isSeen.setVisibility(View.VISIBLE);
                        }else{
                            isSeen.setVisibility(View.INVISIBLE);
                        }

                    } else {
                        if (isTyping_sender) {
                            isTyping.setVisibility(View.VISIBLE);
                        } else {
                            isTyping.setVisibility(View.INVISIBLE);
                        }
                    }
                }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


            /*
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d("AAA","The SnapShot :"+s);
                    if(dataSnapshot.getValue() != null) {
                        HashMap data = (HashMap) dataSnapshot.getValue();
                        if (data.get("text") != null){
                            KeyOfMassage = dataSnapshot.getKey();
                        idSender = data.get("idSender").toString();
                        seen_reciver = (Boolean) data.get("seen_reciver");
                        isTyping_reciver = (Boolean) data.get("isTyping_reciver");
                        isTyping_sender = (Boolean) data.get("isTyping_sender");
                        Backgound_Color = (String) data.get("Background_Color");
                        Log.d("AAA", "Please Work i love u:" + seen_reciver);
                        if (Backgound_Color == null) {
                            Backgound_Color = "";
                        }
                        if (KeyOfMassage != null && idSender != null) {
                            if (!idSender.equals(StaticConfig.UID)) {
                                update_massage.clear();
                                Log.d("AAA", "update value reivce when uer enter :" + idSender);
                                update_massage.put("seen_reciver", true);
                                databaseReference.child("message/" + roomId + "/" + KeyOfMassage).updateChildren(update_massage);
                            } else if (idSender.equals(StaticConfig.UID)) {
                                if (seen_reciver) {
                                    isSeen.setVisibility(View.VISIBLE);
                                } else {
                                    isSeen.setVisibility(View.INVISIBLE);
                                }
                                seen_reciver = false;

                            }
                            Change_Background();

                        }



                    }else{
                            Log.d("AAA","I am config room");
                            Log.d("AAA","frm update sending user :"+isTyping_sender);
                        }
                    }
                  //  Id_sender



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getValue() != null) {
                    HashMap data = (HashMap) dataSnapshot.getValue();
                    KeyOfMassage = dataSnapshot.getKey();
                    idSender = data.get("idSender").toString();
                    seen_reciver = (Boolean) data.get("seen_reciver");
                    isTyping_reciver = (Boolean) data.get("isTyping_reciver");
                    isTyping_sender = (Boolean)data.get("isTyping_sender");
                    Backgound_Color = (String)data.get("Background_Color");
                    Log.d("AAA","After update last node :"+s+"  :Vlaue update :"+seen_reciver);
                    if(Backgound_Color == null){
                        Backgound_Color = "";
                    }
                    Change_Background();

                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


*/
                                        });

        databaseReference.child("blcoks")
                .child(idFriend.get(0).toString())
                .child("Them").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                    while (children.iterator().hasNext()) {
                        DataSnapshot Keys = children.iterator().next();
                        if (Keys.getValue().toString().equals(StaticConfig.UID)) {
                            Intent goToMainActivity = new Intent(ChatActivity.this, MainActivity.class);
                            startActivity(goToMainActivity);

                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        if (idFriend != null && nameFriend != null) {
            getSupportActionBar().setTitle(nameFriend);
            getSupportActionBar().setDisplayOptions(R.id.about);
            linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerChat = (RecyclerView) findViewById(R.id.recyclerChat);
            recyclerChat.setLayoutManager(linearLayoutManager);
            adapter = new ListMessageAdapter(this, consersation, bitmapAvataFriend, bitmapAvataUser,roomId);
            databaseReference.child("message/" + roomId).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(final DataSnapshot dataSnapshot, String s) {
                    if (dataSnapshot.getValue() != null) {
                    HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                        if (mapMessage.containsKey("text")) {
                            update_massage.clear();
                            idSender = (String) mapMessage.get("idSender");
                            Message newMessage = new Message();
                            newMessage.KeyOFMassage = (String) dataSnapshot.getKey();
                            newMessage.idSender = (String) mapMessage.get("idSender");
                            newMessage.idReceiver = (String) mapMessage.get("idReceiver");
                            newMessage.text = (String) mapMessage.get("text");
                            newMessage.timestamp = (long) mapMessage.get("timestamp");
                            consersation.getListMessageData().add(newMessage);
                            if (idSender != null && !idSender.equals("")){
                                Config.clear();
                                Config.put("idSender", newMessage.idSender);
                                databaseReference.child("message").child(roomId).child("ConfigRoom")
                                        .updateChildren(Config);
                            }
                            else if(!idSender.equals(StaticConfig.UID)) {
                                Config.clear();
                                Config.put("seen_reciver", true);
                                databaseReference.child("message").child(roomId).child("ConfigRoom")
                                        .updateChildren(Config);
                            }
                            adapter.notifyDataSetChanged();
                            linearLayoutManager.scrollToPosition(consersation.getListMessageData().size() - 1);
                        }else if(mapMessage.containsKey("image_name")){
                            String imageName =(String)mapMessage.get("image_name");
                            if(imageName.contains(".jpg")){
                           waiting_Images.add(consersation.getListMessageData().size());
                            final String idSender = (String)mapMessage.get("idSender");
                            final long timestamp = (long)mapMessage.get("timestamp");
                            StorageReference imge = storageRef.child("images").child(idSender).child(imageName);
                            imge.getBytes(ImageUtils.ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                @Override
                                public  void onSuccess(byte[] bytes) {
                                    ImageMassage newImage = new ImageMassage();
                                    newImage.idSender =idSender;
                                    newImage.KeyOFMassage =dataSnapshot.getKey().toString();
                                    newImage.timestamp =timestamp;
                                    newImage.Image = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                    consersation.getListMessageData().add(waiting_Images.remove(0),newImage);
                                    adapter.notifyDataSetChanged();
                                    linearLayoutManager.scrollToPosition(consersation.getListMessageData().size() - 1);

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    Message newMessage = new Message();
                                    newMessage.idSender = idSender;
                                    newMessage.text = "Fail Download Image ***** ";
                                    consersation.getListMessageData().add(waiting_Images.remove(0),newMessage);
                                    adapter.notifyDataSetChanged();
                                    linearLayoutManager.scrollToPosition(consersation.getListMessageData().size() - 1);

                                }
                            });
                            }
                        }
                        else{
                            isTyping_reciver = (Boolean) mapMessage.get("isTyping_reciver");
                            isTyping_sender = (Boolean) mapMessage.get("isTyping_sender");
                            Backgound_Color = (String) mapMessage.get("Background_Color");
                            idSender = (String)mapMessage.get("idSender");
                        }

                        // send notification for the user if he is offline or another state
                        databaseReference.child("user").child(idFriend.get(0).toString())
                                .child("status").addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.getValue() !=null) {
                                            HashMap values = (HashMap) dataSnapshot.getValue();
                                            status_my_friend = (Boolean)values.get("isOnline");

                                        }

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });


                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    if(dataSnapshot.getValue() !=null){
                        HashMap updatedMassage = (HashMap)dataSnapshot.getValue();
                        if(!dataSnapshot.getKey().toString().equals("ConfigRoom")){
                            for(int i= 0 ;i<  consersation.getListMessageData().size() ; i++){
                                if(consersation.getListMessageData().get(i).KeyOFMassage.equals(dataSnapshot.getKey().toString())){
                                    consersation.getListMessageData().get(i).text =(String)updatedMassage.get("text") ;
                                    adapter.notifyDataSetChanged();
                                }
                            }


                        }
                        /*
                        String KeyOfMessageDelete = dataSnapshot.getKey().toString();
                        HashMap mapMessage = (HashMap) dataSnapshot.getValue();

                        for(int i= 0 ;i<  consersation.getListMessageData().size() ; i++){
                            Message temp = consersation.getListMessageData().get(i);
                            if(temp.KeyOFMassage.equals(KeyOfMessageDelete)){
                                consersation.getListMessageData().get(i).text = (String)mapMessage.get("text") ;

                            }
                        }
                        */

                    }
/*
                    if(dataSnapshot.getValue() != null){
                       Log.d("AAA","onChildChanged for massage :"+dataSnapshot.toString());
                       if (mapMessage.get("text") != null) {
                           // i reicve this vairable form data base for in another path...
                     //      isTyping_reciver = (Boolean) mapMessage.get("isTyping_reciver");
                     //      isTyping_sender = (Boolean) mapMessage.get("isTyping_sender");
                      //     Backgound_Color = (String) mapMessage.get("Background_Color");
                           }
                       }

**/

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                    if(dataSnapshot.getValue() !=null) {
                        if (!dataSnapshot.getKey().toString().equals("ConfigRoom")) {

                            for (int i = 0; i < consersation.getListMessageData().size(); i++) {
                                if(consersation.getListMessageData().get(i).KeyOFMassage != null) {
                                if (consersation.getListMessageData().get(i).KeyOFMassage.equals(dataSnapshot.getKey().toString())) {
                                    consersation.getListMessageData().remove(i);
                                    adapter.notifyDataSetChanged();
                                  }
                                }
                            }


                          /*  for (int i = 0; i < consersation.getListMessageData().size(); i++) {
                                if (consersation.getListMessageData().get(i).KeyOFMassage.equals(dataSnapshot.getKey().toString())) {
                                    Log.d("DDD","onChildRemoved ==key :"+"==>"+dataSnapshot.toString());
                                //    consersation.getListMessageData().remove(i);
                                //    adapter.notifyDataSetChanged();
                                }
                            }
                            */


                        }
                    }



                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            recyclerChat.setAdapter(adapter);
        }




        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(true)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_custom);
        mFirebaseRemoteConfig.fetch(0)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            mFirebaseRemoteConfig.activateFetched();
                        }
                        remote_config_massage_length();

                    }

                });





    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                Toast.makeText(context, "There are no Image", Toast.LENGTH_LONG).show();
                return;
            }
            try {
                InputStream inputStream = context.getContentResolver().openInputStream(data.getData());

                Bitmap imgBitmap = BitmapFactory.decodeStream(inputStream);
                imgBitmap = ImageUtils.cropToSquare(imgBitmap);
                InputStream is = ImageUtils.convertBitmapToInputStream(imgBitmap);
                final Bitmap liteImage = ImageUtils.makeImageLite(is,
                        imgBitmap.getWidth(), imgBitmap.getHeight(),
                        ImageUtils.AVATAR_WIDTH, ImageUtils.AVATAR_HEIGHT);

                final File result =  ImageUtils.persistImage(context,liteImage, Calendar.getInstance().getTime().getHours()+"_user_image");

                Uri file = Uri.fromFile(result);
                storageRef.child("images").child(StaticConfig.UID).child(result.getName()).putFile(file)
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle unsuccessful uploads
                                Message newMassge = new Message();
                                newMassge.text = "Fail to upload Image ";
                                newMassge.idSender = StaticConfig.UID;
                                newMassge.timestamp = System.currentTimeMillis();
                                consersation.getListMessageData().add(newMassge);
                                adapter.notifyDataSetChanged();
                                linearLayoutManager.scrollToPosition(consersation.getListMessageData().size() - 1);

                            }
                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                        HashMap image =new HashMap();
                        image.put("image_name",result.getName());
                        image.put("idSender",StaticConfig.UID);
                        image.put("timestamp",System.currentTimeMillis());
                        databaseReference.child("message").child(roomId).push().setValue(image);

                        ImageMassage newMassge = new ImageMassage();
                        newMassge.Image = liteImage;
                        newMassge.idSender = StaticConfig.UID;
                        newMassge.timestamp = System.currentTimeMillis();
                        consersation.getListMessageData().add(newMassge);
                        adapter.notifyDataSetChanged();
                    }
                });

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void Change_Background() {


        if (isNoNull(Backgound_Color)){
            Config.put("Backgound_Color", Backgound_Color);
            databaseReference.child("message").child(roomId).child("ConfigRoom").updateChildren(Config);

            switch (Backgound_Color) {
                case ImageUtils.RED:
                    chat_layout.setBackgroundColor(getResources().getColor(R.color.red));
                    break;
                case ImageUtils.GREEN_COLOR:
                    chat_layout.setBackgroundColor(getResources().getColor(R.color.green));
                    break;
                case ImageUtils.BLUE:
                    chat_layout.setBackgroundColor(getResources().getColor(R.color.blue));
                    break;
                case "black":
                    chat_layout.setBackgroundColor(getResources().getColor(R.color.grey_800));
                    break;
                default:
                    break;
            }
    }
    }

    public boolean isNoNull(String var){
        return var!=null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_chat,menu);
         getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent result = new Intent();
            result.putExtra("idFriend", idFriend.get(0));
            setResult(RESULT_OK, result);
            this.finish();
        }
        if(item.getItemId() == R.id.block_user){
            Intent result = new Intent();
            result.putExtra("idFriend", idFriend.get(0));
            setResult(RESULT_OK, result);
            this.finish();
        }
        if(item.getItemId() == R.id.red){
            Backgound_Color = "red";

            Change_Background();
        }
        if(item.getItemId() == R.id.green){
            Backgound_Color = "green";
            Change_Background();
        }
        if(item.getItemId() == R.id.blue){
            Backgound_Color = "blue";
            Change_Background();
        }
        if(item.getItemId() == R.id.black){
            Backgound_Color = "black";
            Change_Background();

        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent result = new Intent();
        result.putExtra("idFriend", idFriend.get(0));
        setResult(RESULT_OK, result);
        this.finish();
    }

    private void remote_config_massage_length() {
         massage_length = Integer.parseInt(mFirebaseRemoteConfig.getString("massage_length"));


    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.btnSend) {
            String content = editWriteMessage.getText().toString().trim();
            if (content.length() > 0 && content.length() <= massage_length) {
                editWriteMessage.setText("");
                Message newMessage = new Message();
                newMessage.text = content;
                newMessage.idSender = StaticConfig.UID;
                newMessage.idReceiver = roomId;
                newMessage.timestamp = System.currentTimeMillis();
                FirebaseDatabase.getInstance().getReference().child("message/" + roomId).push().setValue(newMessage);
                    HashMap ThereAreNewMassage = new HashMap();
                    ThereAreNewMassage.put("massage","There are new Message ");
                    ThereAreNewMassage.put("idSender",StaticConfig.UID);
                    databaseReference.child("notifications").child(idFriend.get(0).toString())
                            .push()
                            .setValue(ThereAreNewMassage);



            }else{
                new AlertDialog.Builder(context)
                        .setTitle("Error")
                        .setMessage("You are write over your limit :"+massage_length)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
            }
        };

            }
        }



class ListMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private Consersation consersation;
    private HashMap<String, Bitmap> bitmapAvata;
    private HashMap<String, DatabaseReference> bitmapAvataDB;
    private Bitmap bitmapAvataUser;
    private String Id_room;
    LovelyProgressDialog dialogWaitDeleting;
    LovelyProgressDialog dialogWait;

    public ListMessageAdapter(Context context, Consersation consersation, HashMap<String, Bitmap> bitmapAvata, Bitmap bitmapAvataUser,String Id_room) {
        this.context = context;
        this.consersation = consersation;
        this.bitmapAvata = bitmapAvata;
        this.bitmapAvataUser = bitmapAvataUser;
        this.Id_room = Id_room;
        bitmapAvataDB = new HashMap<>();
        dialogWaitDeleting = new LovelyProgressDialog(context);
        dialogWait = new LovelyProgressDialog(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ChatActivity.VIEW_TYPE_FRIEND_MESSAGE) {
            View view = LayoutInflater.from(context).inflate(R.layout.rc_item_message_friend, parent, false);
            return new ItemMessageFriendHolder(view);
        } else if (viewType == ChatActivity.VIEW_TYPE_USER_MESSAGE) {
            View view = LayoutInflater.from(context).inflate(R.layout.rc_item_message_user, parent, false);
            return new ItemMessageUserHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ItemMessageFriendHolder) {


            if(!(consersation.getListMessageData().get(position) instanceof ImageMassage)) {
                ((ItemMessageFriendHolder) holder).txtContent.setText(consersation.getListMessageData().get(position).text);
                ((ItemMessageFriendHolder) holder).txtContent.setVisibility(View.VISIBLE);
                ((ItemMessageFriendHolder) holder).Image.setImageBitmap(null);
                ((ItemMessageFriendHolder) holder).Image.setVisibility(View.INVISIBLE);


            }else{
                ImageMassage showImage = (ImageMassage)consersation.getListMessageData().get(position);
                ((ItemMessageFriendHolder) holder).Image.setImageBitmap(showImage.Image);
                ((ItemMessageFriendHolder) holder).Image.setVisibility(View.VISIBLE);
                ((ItemMessageFriendHolder) holder).txtContent.setVisibility(View.INVISIBLE);

            }
            Bitmap currentAvata = bitmapAvata.get(consersation.getListMessageData().get(position).idSender);
            if (currentAvata != null) {
                ((ItemMessageFriendHolder) holder).avata.setImageBitmap(currentAvata);
            } else {

                final String id = consersation.getListMessageData().get(position).idSender;
                if(bitmapAvataDB.get(id) == null){
                    bitmapAvataDB.put(id, FirebaseDatabase.getInstance().getReference().child("user/" + id + "/avata"));
                    bitmapAvataDB.get(id).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getValue() != null) {
                                String avataStr = (String) dataSnapshot.getValue();
                                if(!avataStr.equals(StaticConfig.STR_DEFAULT_BASE64)) {
                                    StorageReference imge = FirebaseStorage.getInstance().getReference().child("images").child(id).child(avataStr);
                                    imge.getBytes(ImageUtils.ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                        @Override
                                        public  void onSuccess(byte[] bytes) {
                                            // Data for "images/island.jpg" is returns, use this as needed
                                            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                            ChatActivity.bitmapAvataFriend.put(id,bitmap);
                                            notifyDataSetChanged();
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception exception) {
                                            // Handle any errors
                                        }
                                    });
                                  //  byte[] decodedString = Base64.decode(avataStr, Base64.DEFAULT);
                                  //  ChatActivity.bitmapAvataFriend.put(id, BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length));
                                }else{
                                    ChatActivity.bitmapAvataFriend.put(id, BitmapFactory.decodeResource(context.getResources(), R.drawable.default_avata));
                                }

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }
        } else if (holder instanceof ItemMessageUserHolder) {
            ((View)((ItemMessageUserHolder) holder).txtContent.getParent()).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    PopupMenu pop = new PopupMenu(context,((ItemMessageUserHolder) holder).txtContent);
                    pop.getMenuInflater().inflate(R.menu.menu_massage_action,pop.getMenu());
                    pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int id = item.getItemId();
                            switch (id){
                                case R.id.update_massage:
                               {
                                    String temp = ((ItemMessageUserHolder) holder).txtContent.getText().toString();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);

// Set up the input
                                     final EditText input = new EditText(context);
                                    input.setText(temp);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                                    builder.setView(input);

// Set up the buttons
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            final String text = input.getText().toString();
                                            HashMap update_massage = new HashMap();
                                            update_massage.put("text",text);
                                            FirebaseDatabase.getInstance().getReference().child("message")
                                                    .child(Id_room).child(consersation.getListMessageData().get(position).KeyOFMassage).updateChildren(update_massage)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            dialogWaitDeleting.dismiss();
                                                            new LovelyInfoDialog(context)
                                                                    .setTopColorRes(R.color.colorAccent)
                                                                    .setTitle("Success")
                                                                    .setMessage("message updated successfully")
                                                                    .show();



                                                        }
                                                    })
                                                    .addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            dialogWaitDeleting.dismiss();
                                                            new LovelyInfoDialog(context)
                                                                    .setTopColorRes(R.color.colorAccent)
                                                                    .setTitle("Error")
                                                                    .setMessage("Error occurred during updating message")
                                                                    .show();
                                                        }
                                                    });
                                        }
                                    });
                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                                    builder.show();

                                        }

                                    break;
                                case R.id.delete_massage:
                                    Toast.makeText(context, "Delete Massage", Toast.LENGTH_SHORT).show();
                                                FirebaseDatabase.getInstance().getReference().child("message")
                                                        .child(Id_room).child(consersation.getListMessageData().get(position).KeyOFMassage).removeValue()
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                dialogWaitDeleting.dismiss();
                                                                new LovelyInfoDialog(context)
                                                                        .setTopColorRes(R.color.colorAccent)
                                                                        .setTitle("Success")
                                                                        .setMessage("message deleting successfully")
                                                                        .show();


                                                            }
                                                        })
                                                        .addOnFailureListener(new OnFailureListener() {
                                                            @Override
                                                            public void onFailure(@NonNull Exception e) {
                                                                dialogWaitDeleting.dismiss();
                                                                new LovelyInfoDialog(context)
                                                                        .setTopColorRes(R.color.colorAccent)
                                                                        .setTitle("Error")
                                                                        .setMessage("Error occurred during deleting message")
                                                                        .show();
                                                            }
                                                        });


                                    break;
                            }
                            return true;
                        }
                    });
                    pop.show();

                    return true;
                }

            });
            if(!(consersation.getListMessageData().get(position) instanceof ImageMassage)) {
                ((ItemMessageUserHolder) holder).txtContent.setText(consersation.getListMessageData().get(position).text);
                ((ItemMessageUserHolder) holder).txtContent.setVisibility(View.VISIBLE);
                ((ItemMessageUserHolder) holder).Image.setImageBitmap(null);
                ((ItemMessageUserHolder) holder).Image.setVisibility(View.VISIBLE);


            }else{
                ImageMassage showImage = (ImageMassage)consersation.getListMessageData().get(position);
                ((ItemMessageUserHolder) holder).Image.setImageBitmap(showImage.Image);
                ((ItemMessageUserHolder) holder).txtContent.setVisibility(View.INVISIBLE);
                ((ItemMessageUserHolder) holder).Image.setVisibility(View.VISIBLE);

            }
            if (bitmapAvataUser != null) {
                ((ItemMessageUserHolder) holder).avata.setImageBitmap(bitmapAvataUser);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return consersation.getListMessageData().get(position).idSender.equals(StaticConfig.UID) ? ChatActivity.VIEW_TYPE_USER_MESSAGE : ChatActivity.VIEW_TYPE_FRIEND_MESSAGE;
    }

    @Override
    public int getItemCount() {
        return consersation.getListMessageData().size();
    }
}

class ItemMessageUserHolder extends RecyclerView.ViewHolder {
    public TextView txtContent;
    public CircleImageView avata;
    public ImageView Image;

    public ItemMessageUserHolder(View itemView) {
        super(itemView);
        txtContent = (TextView) itemView.findViewById(R.id.textContentUser);
        avata = (CircleImageView) itemView.findViewById(R.id.imageView2);
        Image = (ImageView) itemView.findViewById(R.id.imageView4);
    }
}

class ItemMessageFriendHolder extends RecyclerView.ViewHolder {
    public TextView txtContent;
    public CircleImageView avata;
    public ImageView  Image;

    public ItemMessageFriendHolder(View itemView) {
        super(itemView);
        txtContent = (TextView) itemView.findViewById(R.id.textContentFriend);
        avata = (CircleImageView) itemView.findViewById(R.id.imageView3);
        Image = (ImageView) itemView.findViewById(R.id.imageView);
    }
}

