package com.example.hazem.tryfirebase.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;

import com.example.hazem.tryfirebase.model.User;
import com.google.gson.Gson;


public class SharedPreferenceHelper {
    private static SharedPreferenceHelper instance = null;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;
    private static String SHARE_USER_INFO = "userinfo";
    private static String SHARE_KEY_NAME = "name";
    private static String SHARE_KEY_EMAIL = "email";
    private static String SHARE_KEY_AVATA = "avata";
    private static String SHARE_KEY_myImage = "myImage";
    private static String SHARE_KEY_Length_massage = "length_massage";
    private static String SHARE_KEY_DEVICE_TOKEN = "device_token";

    private static String SHARE_KEY_UID = "uid";
    Gson gson = new Gson();


    private SharedPreferenceHelper() {}

    public static SharedPreferenceHelper getInstance(Context context) {
        if (instance == null) {
            instance = new SharedPreferenceHelper();
            preferences = context.getSharedPreferences(SHARE_USER_INFO, Context.MODE_PRIVATE);
            editor = preferences.edit();
        }
        return instance;
    }

    public void saveUserInfo(User user) {
        String myImage = gson.toJson(user.myImage);
        editor.putString(SHARE_KEY_DEVICE_TOKEN,user.device_token);
        editor.putString(SHARE_KEY_NAME, user.name);
        editor.putString(SHARE_KEY_EMAIL, user.email);
        editor.putString(SHARE_KEY_AVATA, user.avata);
        editor.putString(SHARE_KEY_UID, StaticConfig.UID);
        editor.putString(SHARE_KEY_myImage,myImage);
        editor.putInt(SHARE_KEY_Length_massage,user.length_massage);
        editor.apply();
    }

    public User getUserInfo(){
        String userName = preferences.getString(SHARE_KEY_NAME, "");
        String device_token = preferences.getString(SHARE_KEY_DEVICE_TOKEN, "");
        String email = preferences.getString(SHARE_KEY_EMAIL, "");
        String avatar = preferences.getString(SHARE_KEY_AVATA, "default");
        Bitmap myImage = gson.fromJson(preferences.getString(SHARE_KEY_myImage,""),Bitmap.class);
        int length_massage = preferences.getInt(SHARE_KEY_Length_massage,300);

        User user = new User();
        user.device_token = device_token;
        user.name = userName;
        user.email = email;
        user.avata = avatar;
        user.myImage = myImage;
        user.length_massage = length_massage;

        return user;
    }

    public String getUID(){
        return preferences.getString(SHARE_KEY_UID, "");
    }

}
