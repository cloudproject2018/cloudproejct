package com.example.hazem.tryfirebase.model;


import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;
@IgnoreExtraProperties
public class Status{
    public boolean isOnline;
    public String status;
    public long timestamp;

    public Status(){
        isOnline = false;
        timestamp = 0;
        status = "offline";
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("isOnline", isOnline);
        result.put("status", status);
        result.put("timestamp", timestamp);
        return result;
    }
}
