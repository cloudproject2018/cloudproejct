package com.example.hazem.tryfirebase.model;


import android.graphics.Bitmap;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;
@IgnoreExtraProperties
public class Message{
    public String KeyOFMassage;
    public String idSender;
    public String idReceiver;
    public String text;
    public long timestamp;

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("KeyOFMassage",KeyOFMassage);
        result.put("idSender", idSender);
        result.put("idReceiver", idReceiver);
        result.put("text", text);
        result.put("timestamp", timestamp);
        return result;
    }

    @Override
    public String toString() {
        return "{KeyOFMassage:"+KeyOFMassage+", "+"idSender: "+idSender+" , idReceiver :"+idReceiver+", text:"+text+"  ,timestamp :"+timestamp+"}";
    }
}