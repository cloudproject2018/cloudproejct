package com.example.hazem.tryfirebase.model;


import android.graphics.Bitmap;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class User {
    public String device_token;
    public String name;
    public String email;
    public String avata;
    public Status status;
    public Message message;
    public Bitmap myImage;
    public int length_massage ;


    public User(){
        status = new Status();
        message = new Message();
        status.isOnline = false;
        status.status = "offline";
        status.timestamp = 0;
        message.idReceiver = "0";
        message.idSender = "0";
        message.text = "";
        message.timestamp = 0;
        length_massage = 300;
        myImage = null;
        device_token = "";
    }
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("device_token",device_token);
        result.put("name", name);
        result.put("email", email);
        result.put("avata", avata);
        result.put("status", status);
        result.put("message", message);
        result.put("myImage",myImage);
        result.put("length_massage",length_massage);
        return result;
    }
}
