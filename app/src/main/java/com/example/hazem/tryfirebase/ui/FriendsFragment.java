package com.example.hazem.tryfirebase.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hazem.tryfirebase.R;
import com.example.hazem.tryfirebase.model.Status;
import com.example.hazem.tryfirebase.util.ImageUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.example.hazem.tryfirebase.data.StaticConfig;
import com.example.hazem.tryfirebase.model.Friend;
import com.example.hazem.tryfirebase.model.ListFriend;
import com.example.hazem.tryfirebase.service.ServiceUtils;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import de.hdodenhof.circleimageview.CircleImageView;

public class FriendsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private RecyclerView recyclerListFrends;
    private ListFriendsAdapter adapter;
    public FragFriendClickFloatButton onClickFloatButton;
    public  ListFriend dataListFriend = null;
    private ArrayList<String> listFriendID = null;
    private HashMap listBlockedID = null;
    private HashMap listBlockedME = null;
    private LovelyProgressDialog dialogFindAllFriend;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CountDownTimer detectFriendOnline;
    public static int ACTION_START_CHAT = 1;
    private static FriendsFragment instance = null;
    private DatabaseReference mdatabase = FirebaseDatabase.getInstance().getReference();

    public static final String ACTION_DELETE_FRIEND = "com.android.rivchat.DELETE_FRIEND";

    private BroadcastReceiver deleteFriendReceiver;

    public static FriendsFragment getInstance(){
        if(instance == null) instance = new FriendsFragment();

        return instance;
    }


    public FriendsFragment() {
        dataListFriend = new ListFriend();
        instance = this;
        onClickFloatButton = new FragFriendClickFloatButton();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);


    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        detectFriendOnline = new CountDownTimer(System.currentTimeMillis(), StaticConfig.TIME_TO_REFRESH) {
            @Override
            public void onTick(long l) {
                ServiceUtils.updateFriendStatus(getContext(), dataListFriend);
                ServiceUtils.updateUserStatus(getContext());
            }

            @Override
            public void onFinish() {

            }
        };
   //     if (dataListFriend == null) {
   //         if (dataListFriend.getListFriend().size() > 0) {
    //            listFriendID = new ArrayList<>();
    //            for (Friend friend : dataListFriend.getListFriend()) {
    //                listFriendID.add(friend.id);
    //            }
                detectFriendOnline.start();
    //        }
    //    }
        View layout = inflater.inflate(R.layout.fragment_people, container, false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerListFrends = (RecyclerView) layout.findViewById(R.id.recycleListFriend);
        recyclerListFrends.setLayoutManager(linearLayoutManager);
        mSwipeRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        if(listBlockedID == null){
            listBlockedID = new HashMap();
            listBlockedME = new HashMap();
            getListBlockedUId();
        }
        adapter = new ListFriendsAdapter(getContext(), dataListFriend, this,listBlockedID,listBlockedME);
        recyclerListFrends.setAdapter(adapter);

        dialogFindAllFriend = new LovelyProgressDialog(getContext());
        if (listFriendID == null) {
            listFriendID = new ArrayList<>();
            dialogFindAllFriend.setCancelable(false)
                    .setIcon(R.drawable.ic_add_friend)
                    .setTitle("Get all friend....")
                    .setTopColorRes(R.color.colorPrimary)
                    .show();
            getListFriendUId();
        }




        deleteFriendReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String idDeleted = intent.getExtras().getString("idFriend");
                for (Friend friend : dataListFriend.getListFriend()) {
                    if(idDeleted.equals(friend.id)){
                        ArrayList<Friend> friends = dataListFriend.getListFriend();
                        friends.remove(friend);
                       int index = listFriendID.indexOf(friend.id);
                        listFriendID.remove(index);
                        break;
                    }
                }
                adapter.notifyDataSetChanged();
            }
        };

        IntentFilter intentFilter = new IntentFilter(ACTION_DELETE_FRIEND);
        getContext().registerReceiver(deleteFriendReceiver, intentFilter);

        return layout;
    }

    @Override
    public void onDestroyView (){
        super.onDestroyView();

        getContext().unregisterReceiver(deleteFriendReceiver);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getActivity().getMenuInflater().inflate(R.menu.menu_action_friend,menu);
       //         getMenuInflater().inflate(R.menu.actions , menu);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (ACTION_START_CHAT == requestCode && data != null && ListFriendsAdapter.mapMark != null) {
            ListFriendsAdapter.mapMark.put(data.getStringExtra("idFriend"), false);
        }
    }

    @Override
    public void onRefresh() {
        listFriendID.clear();
        listBlockedID.clear();
        listBlockedME.clear();
        dataListFriend.getListFriend().clear();
        detectFriendOnline.cancel();
        getListFriendUId();
        getListBlockedUId();
        adapter.notifyDataSetChanged();

    }

    public class FragFriendClickFloatButton implements View.OnClickListener {
        Context context;
        LovelyProgressDialog dialogWait;

        public FragFriendClickFloatButton() {
        }

        public FragFriendClickFloatButton getInstance(Context context) {
            this.context = context;
            dialogWait = new LovelyProgressDialog(context);
            return this;
        }

        @Override
        public void onClick(final View view) {
            new LovelyTextInputDialog(view.getContext(), R.style.EditTextTintTheme)
                    .setTopColorRes(R.color.colorPrimary)
                    .setTitle("Add friend")
                    .setMessage("Enter friend email")
                    .setIcon(R.drawable.ic_add_friend)
                    .setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
                    .setInputFilter("Email not found", new LovelyTextInputDialog.TextFilter() {
                        @Override
                        public boolean check(String text) {
                            Pattern VALID_EMAIL_ADDRESS_REGEX =
                                    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
                            Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(text);
                            return matcher.find();
                        }
                    })
                    .setConfirmButton(android.R.string.ok, new LovelyTextInputDialog.OnTextInputConfirmListener() {
                        @Override
                        public void onTextInputConfirmed(String text) {
                            //Tim id user id
                            findIDEmail(text);
                            //Check xem da ton tai ban ghi friend chua
                            //Ghi them 1 ban ghi
                        }
                    })
                    .show();
        }

        /**
         * TIm id cua email tren server
         *
         * @param email
         */
        private void findIDEmail(String email) {
            dialogWait.setCancelable(false)
                    .setIcon(R.drawable.ic_add_friend)
                    .setTitle("Finding friend....")
                    .setTopColorRes(R.color.colorPrimary)
                    .show();
            mdatabase.child("user").orderByChild("email").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    dialogWait.dismiss();
                    if (dataSnapshot.getValue() == null) {
                        //email not found
                        new LovelyInfoDialog(context)
                                .setTopColorRes(R.color.colorAccent)
                                .setIcon(R.drawable.ic_add_friend)
                                .setTitle("Fail")
                                .setMessage("Email not found")
                                .show();
                    } else {
                        String id = ((HashMap) dataSnapshot.getValue()).keySet().iterator().next().toString();
                        if (id.equals(StaticConfig.UID)) {
                            new LovelyInfoDialog(context)
                                    .setTopColorRes(R.color.colorAccent)
                                    .setIcon(R.drawable.ic_add_friend)
                                    .setTitle("Fail")
                                    .setMessage("Email not valid")
                                    .show();
                        } else {
                            HashMap userMap = (HashMap) ((HashMap) dataSnapshot.getValue()).get(id);
                            Friend user = new Friend();
                            user.name = (String) userMap.get("name");
                            user.email = (String) userMap.get("email");
                            user.avata = (String) userMap.get("avata");
                            user.id = id;
                            user.idRoom = id.compareTo(StaticConfig.UID) > 0 ? (StaticConfig.UID + id).hashCode() + "" : "" + (id + StaticConfig.UID).hashCode();
                            checkBeforAddFriend(id, user);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }


        private void checkBeforAddFriend(final String idFriend, Friend userInfo) {
            dialogWait.setCancelable(false)
                    .setIcon(R.drawable.ic_add_friend)
                    .setTitle("Add friend....")
                    .setTopColorRes(R.color.colorPrimary)
                    .show();

            //Check xem da ton tai id trong danh sach id chua
            if (listFriendID.contains(idFriend)) {
                dialogWait.dismiss();
                new LovelyInfoDialog(context)
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(R.drawable.ic_add_friend)
                        .setTitle("Friend")
                        .setMessage("User "+userInfo.email + " has been friend")
                        .show();
            } else {
                addFriend(idFriend, true);
                listFriendID.add(idFriend);
                dataListFriend.getListFriend().add(userInfo);
                adapter.notifyDataSetChanged();
            }
        }


        private void addFriend(final String idFriend, boolean isIdFriend) {
            if (idFriend != null) {
                if (isIdFriend) {
                    mdatabase.child("friend/" + StaticConfig.UID).push().setValue(idFriend)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        addFriend(idFriend, false);
                                    }
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    dialogWait.dismiss();
                                    new LovelyInfoDialog(context)
                                            .setTopColorRes(R.color.colorAccent)
                                            .setIcon(R.drawable.ic_add_friend)
                                            .setTitle("False")
                                            .setMessage("False to add friend success")
                                            .show();
                                }
                            });
                } else {
                    mdatabase.child("friend/" + idFriend).push().setValue(StaticConfig.UID).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                addFriend(null, false);
                            }
                        }
                    })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    dialogWait.dismiss();
                                    new LovelyInfoDialog(context)
                                            .setTopColorRes(R.color.colorAccent)
                                            .setIcon(R.drawable.ic_add_friend)
                                            .setTitle("False")
                                            .setMessage("False to add friend success")
                                            .show();
                                }
                            });
                }
            } else {
                dialogWait.dismiss();
                new LovelyInfoDialog(context)
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(R.drawable.ic_add_friend)
                        .setTitle("Success")
                        .setMessage("Add friend success")
                        .show();
            }
        }


    }



        /**
         * Add friend
         *
         * @param idFriend
         */
        /*
        private void addFriend(final String idFriend, boolean isIdFriend) {
            if (idFriend != null) {
                if (isIdFriend) {
                    FirebaseDatabase.getInstance().getReference().child("friend/" + StaticConfig.UID).push().setValue(idFriend)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        addFriend(idFriend, false);
                                    }
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    dialogWait.dismiss();
                                    new LovelyInfoDialog(context)
                                            .setTopColorRes(R.color.colorAccent)
                                            .setIcon(R.drawable.ic_add_friend)
                                            .setTitle("False")
                                            .setMessage("False to add friend success")
                                            .show();
                                }
                            });
                }
            } else {
                dialogWait.dismiss();
                new LovelyInfoDialog(context)
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(R.drawable.ic_add_friend)
                        .setTitle("Success")
                        .setMessage("Add friend success")
                        .show();
            }
        }


    }
*/
    /**
     * Lay danh sach ban be tren server
     */
    private void getListFriendUId() {
        mdatabase.child("friend/" + StaticConfig.UID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    HashMap mapRecord = (HashMap) dataSnapshot.getValue();
                    Iterator listKey = mapRecord.keySet().iterator();
                    while (listKey.hasNext()) {
                        String key = listKey.next().toString();
                        listFriendID.add(mapRecord.get(key).toString());
                    }
                    getAllFriendInfo(0);
                } else {
                    dialogFindAllFriend.dismiss();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void getListBlockedUId() {



        mdatabase.child("blcoks")
                 .child(StaticConfig.UID)
                 .child("Them").
                        addListenerForSingleValueEvent(
                                new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.getValue() != null) {
                                            Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                                            while (children.iterator().hasNext()) {
                                                DataSnapshot Keys = children.iterator().next();
                                                listBlockedID.put(Keys.getKey(),Keys.getValue());
                                            }
                                        }

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });

        mdatabase.child("blcoks")
                .child(StaticConfig.UID+"/ME").
                addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.getValue() != null) {
                                    Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                                    while (children.iterator().hasNext()) {
                                        DataSnapshot Keys = children.iterator().next();
                                        listBlockedME.put(Keys.getKey(),Keys.getValue());
                                    }
                                }

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
    }

    /**
     * Truy cap bang user lay thong tin id nguoi dung
     */
    Friend temp_user;
    private void getAllFriendInfo(final int index) {
        if (index == listFriendID.size()) {
            //save list friend
            adapter.notifyDataSetChanged();
            dialogFindAllFriend.dismiss();
            mSwipeRefreshLayout.setRefreshing(false);
            detectFriendOnline.start();
        } else {
            final String id = listFriendID.get(index);
            mdatabase.child("user/" + id).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {
                        temp_user = new Friend();
                        HashMap mapUserInfo = (HashMap) dataSnapshot.getValue();
                        temp_user.name = (String) mapUserInfo.get("name");
                        temp_user.email = (String) mapUserInfo.get("email");
                        temp_user.avata = (String) mapUserInfo.get("avata");
                        HashMap Status = (HashMap)mapUserInfo.get("status");
                        temp_user.status.isOnline = (Boolean)Status.get("isOnline");
                        temp_user.status.status = (String)Status.get("status");
                        temp_user.status.timestamp = (long)Status.get("timestamp");
                        if(temp_user.avata.equals(StaticConfig.STR_DEFAULT_BASE64)) {
                            temp_user.id = id;
                            temp_user.idRoom = id.compareTo(StaticConfig.UID) > 0 ? (StaticConfig.UID + id).hashCode() + "" : "" + (id + StaticConfig.UID).hashCode();
                            dataListFriend.getListFriend().add(temp_user);
                            getAllFriendInfo(index + 1);
                        }else{
                            StorageReference imge = FirebaseStorage.getInstance().getReference().child("images").child(id).child(temp_user.avata);
                             imge.getBytes(ImageUtils.ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                @Override
                                public  void onSuccess(byte[] bytes) {
                                    // Data for "images/island.jpg" is returns, use this as needed

                                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                    temp_user.id = id;
                                    temp_user.myImage = bitmap;
                                    temp_user.idRoom = id.compareTo(StaticConfig.UID) > 0 ? (StaticConfig.UID + id).hashCode() + "" : "" + (id + StaticConfig.UID).hashCode();
                                    dataListFriend.getListFriend().add(temp_user);
                                    getAllFriendInfo(index + 1);

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle any errors
                                    temp_user.id = id;
                                    temp_user.avata = StaticConfig.STR_DEFAULT_BASE64;
                                    temp_user.idRoom = id.compareTo(StaticConfig.UID) > 0 ? (StaticConfig.UID + id).hashCode() + "" : "" + (id + StaticConfig.UID).hashCode();

                                    dataListFriend.getListFriend().add(temp_user);
                                    getAllFriendInfo(index + 1);
                                }
                            });



                        }
                        // here it is move some code ____
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    public  ListFriend getdataListFriend(){
        return dataListFriend;
    }
}

class ListFriendsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ListFriend listFriend;
    private HashMap listBlockedID;
    private HashMap listBlockedME;
    private Context context;
    public static Map<String, Query> mapQuery;
    public static Map<String, DatabaseReference> mapQueryOnline;
    public static Map<String, ChildEventListener> mapChildListener;
    public static Map<String, ChildEventListener> mapChildListenerOnline;
    public static Map<String, Boolean> mapMark;
    private FriendsFragment fragment;
    LovelyProgressDialog dialogWaitDeleting;
    LovelyProgressDialog dialogWait;

    public ListFriendsAdapter(Context context, ListFriend listFriend, FriendsFragment fragment,HashMap listBlockedID,HashMap listBlockedME) {

        this.listFriend = listFriend;
        this.listBlockedID = listBlockedID;
        this.listBlockedME = listBlockedME;

        this.context = context;
        mapQuery = new HashMap<>();
        mapChildListener = new HashMap<>();
        mapMark = new HashMap<>();
        mapChildListenerOnline = new HashMap<>();
        mapQueryOnline = new HashMap<>();
        this.fragment = fragment;
        dialogWaitDeleting = new LovelyProgressDialog(context);
        dialogWait = new LovelyProgressDialog(context);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rc_item_friend, parent, false);
        return new ItemFriendViewHolder(context, view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final String name = listFriend.getListFriend().get(position).name;
        final String id = listFriend.getListFriend().get(position).id;
        final String idRoom = listFriend.getListFriend().get(position).idRoom;
        final String avata = listFriend.getListFriend().get(position).avata;
        final String status = listFriend.getListFriend().get(position).status.status;
        final Bitmap myimg = listFriend.getListFriend().get(position).myImage;
        if(listBlockedID.containsValue(id)){
            ((ItemFriendViewHolder) holder).status_blocked.setVisibility(View.VISIBLE);
        }else if (!listBlockedID.containsValue(id)){
            ((ItemFriendViewHolder) holder).status_blocked.setVisibility(View.INVISIBLE);
        }

        else if(listBlockedME.containsValue(StaticConfig.UID)){
            ((ItemFriendViewHolder) holder).status_blocked.setVisibility(View.VISIBLE);
        }else if (!listBlockedME.containsValue(StaticConfig.UID)){
            ((ItemFriendViewHolder) holder).status_blocked.setVisibility(View.INVISIBLE);
        }
        ((ItemFriendViewHolder) holder).txtName.setText(name);

        ((View) ((ItemFriendViewHolder) holder).txtName.getParent().getParent().getParent())
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!listBlockedID.containsValue(id) && !listBlockedME.containsValue(StaticConfig.UID)){
                            ((ItemFriendViewHolder) holder).txtName.setTypeface(Typeface.DEFAULT);
                        Intent intent = new Intent(context, ChatActivity.class);
                        intent.putExtra(StaticConfig.INTENT_KEY_CHAT_FRIEND, name);
                        ArrayList<CharSequence> idFriend = new ArrayList<CharSequence>();
                        idFriend.add(id);
                        intent.putCharSequenceArrayListExtra(StaticConfig.INTENT_KEY_CHAT_ID, idFriend);
                        intent.putExtra(StaticConfig.INTENT_KEY_CHAT_ROOM_ID, idRoom);
                        ChatActivity.bitmapAvataFriend = new HashMap<>();
                        if (!avata.equals(StaticConfig.STR_DEFAULT_BASE64)) {
                            ChatActivity.bitmapAvataFriend.put(id, myimg);
                        } else {
                            ChatActivity.bitmapAvataFriend.put(id, BitmapFactory.decodeResource(context.getResources(), R.drawable.default_avata));
                        }

                        mapMark.put(id, null);
                        fragment.startActivityForResult(intent, FriendsFragment.ACTION_START_CHAT);
                    }else{
                            Toast.makeText(context, "You are blocked  user ", Toast.LENGTH_LONG).show();
                        }
                    }
                });

        ((View) ((ItemFriendViewHolder) holder).txtName.getParent().getParent().getParent())
                .setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        final String friendName = (String) ((ItemFriendViewHolder) holder).txtName.getText();
                        PopupMenu pop = new PopupMenu(context,((View) ((ItemFriendViewHolder) holder).txtName.getParent().getParent().getParent()));
                        pop.getMenuInflater().inflate(R.menu.menu_action_friend,pop.getMenu());
                        pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                int id = item.getItemId();
                                switch (id){
                                    case R.id.delete_user:
                                        new AlertDialog.Builder(context)
                                                .setTitle("Delete Friend")
                                                .setMessage("Are you sure want to delete " + friendName + "?")
                                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.dismiss();
                                                        final String idFriendRemoval = listFriend.getListFriend().get(position).id;
                                                        dialogWaitDeleting.setTitle("Deleting...")
                                                                .setCancelable(false)
                                                                .setTopColorRes(R.color.colorAccent)
                                                                .show();
                                                        deleteFriend(idFriendRemoval);
                                                    }
                                                })
                                                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.dismiss();
                                                    }
                                                }).show();

                                        break;
                                    case R.id.block_user:
                                        new AlertDialog.Builder(context)
                                                .setTitle("Block Friend")
                                                .setMessage("Are you sure want to Block " + friendName + "?")
                                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.dismiss();
                                                        final String idFriendBlocked = listFriend.getListFriend().get(position).id;
                                                        dialogWaitDeleting.setTitle("Blocking...")
                                                                .setCancelable(false)
                                                                .setTopColorRes(R.color.colorAccent)
                                                                .show();
                                                        checkBeforBlockFriend(idFriendBlocked);
                                                    }
                                                })
                                                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.dismiss();
                                                    }
                                                }).show();

                                        break;


                                    case R.id.unblock_user:
                                        new AlertDialog.Builder(context)
                                                .setTitle("unBlock Friend")
                                                .setMessage("Are you sure want to unBlock " + friendName + "?")
                                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.dismiss();
                                                        final String idFriendBlocked = listFriend.getListFriend().get(position).id;
                                                        dialogWaitDeleting.setTitle("unBlocking...")
                                                                .setCancelable(false)
                                                                .setTopColorRes(R.color.colorAccent)
                                                                .show();
                                                               unblockFriend(idFriendBlocked,true);
                                                    }
                                                })
                                                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.dismiss();
                                                    }
                                                }).show();

                                        break;
                                }
                                return true;
                            }
                        });
                        pop.show();


                        return true;
                    }
                });

/***********************************************************/

        if (listFriend.getListFriend().size() > 0) {

            String time = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(listFriend.getListFriend().get(position).message.timestamp));
            String today = new SimpleDateFormat("EEE, d MMM yyyy").format(new Date(System.currentTimeMillis()));
            if (today.equals(time)) {
                ((ItemFriendViewHolder) holder).txtTime.setText(new SimpleDateFormat("HH:mm").format(new Date(listFriend.getListFriend().get(position).message.timestamp)));
            } else {
                ((ItemFriendViewHolder) holder).txtTime.setText(new SimpleDateFormat("MMM d").format(new Date(listFriend.getListFriend().get(position).message.timestamp)));
            }
        } else {
            ((ItemFriendViewHolder) holder).txtTime.setVisibility(View.GONE);
            if (mapQuery.get(id) == null && mapChildListener.get(id) == null) {
                mapQuery.put(id, FirebaseDatabase.getInstance().getReference().child("message/" + idRoom).limitToLast(2));
                mapChildListener.put(id, new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                        if (mapMessage !=null ){
                            if(!mapMessage.containsKey("isTyping_sender")){
                                if (listFriend.getListFriend().size() != 0) {
                                    listFriend.getListFriend().get(position).message.timestamp = (long) mapMessage.get("timestamp");
                                }
                            }


                       }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                mapQuery.get(id).addChildEventListener(mapChildListener.get(id));
                mapMark.put(id, true);
            } else {
                mapQuery.get(id).removeEventListener(mapChildListener.get(id));
                mapQuery.get(id).addChildEventListener(mapChildListener.get(id));
                mapMark.put(id, true);
            }

            }
            /********************************************/
            if (listFriend.getListFriend().get(position).avata.equals(StaticConfig.STR_DEFAULT_BASE64)) {
                ((ItemFriendViewHolder) holder).avata.setImageResource(R.drawable.default_avata);
            } else {
           //     byte[] decodedString = Base64.decode(listFriend.getListFriend().get(position).avata, Base64.DEFAULT);
           //     Bitmap src = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                ((ItemFriendViewHolder) holder).avata.setImageBitmap(myimg);
            }


            if (mapQueryOnline.get(id) == null && mapChildListenerOnline.get(id) == null) {
            mapQueryOnline.put(id, FirebaseDatabase.getInstance().getReference().child("user/" + id + "/status"));
            mapChildListenerOnline.put(id, new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    if (dataSnapshot.getValue() != null && dataSnapshot.getKey().equals("status")) {
                        Log.d("FriendsFragment add " + id, (String) dataSnapshot.getValue() + "");
                        listFriend.getListFriend().get(position).status.status = (String) dataSnapshot.getValue();
                        ((ItemFriendViewHolder) holder).status_string.setText(status);
                         notifyDataSetChanged();
                    }

                    if (dataSnapshot.getValue() != null && dataSnapshot.getKey().equals("isOnline")) {
                        Log.d("FriendsFragment add " + id, (boolean) dataSnapshot.getValue() + "");
                        listFriend.getListFriend().get(position).status.isOnline = (boolean) dataSnapshot.getValue();
                       notifyDataSetChanged();
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    if (dataSnapshot.getValue() != null && dataSnapshot.getKey().equals("status")) {
                        Log.d("FriendsFragment add " + id, (String) dataSnapshot.getValue() + "");
                        listFriend.getListFriend().get(position).status.status = (String) dataSnapshot.getValue();
                        ((ItemFriendViewHolder) holder).status_string.setText(status);
                       notifyDataSetChanged();
                    }


                    if (dataSnapshot.getValue() != null && dataSnapshot.getKey().equals("isOnline")) {
                        Log.d("FriendsFragment change " + id, (boolean) dataSnapshot.getValue() + "");
                        listFriend.getListFriend().get(position).status.isOnline = (boolean) dataSnapshot.getValue();

                        notifyDataSetChanged();
                    }

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            mapQueryOnline.get(id).addChildEventListener(mapChildListenerOnline.get(id));
        }



            if (listFriend.getListFriend().get(position).status.isOnline) {
                ((ItemFriendViewHolder) holder).avata.setBorderWidth(10);
            //    ((ItemFriendViewHolder) holder).status_string.setText("Online");
            } else {
                ((ItemFriendViewHolder) holder).avata.setBorderWidth(0);
                ((ItemFriendViewHolder) holder).status_string.setText("Offline");
            }
        }

    @Override
    public int getItemCount() {
        return listFriend.getListFriend() != null ? listFriend.getListFriend().size() : 0;
    }

    /**
     * Delete friend
     *
     * @param idFriend
     */
    private void deleteFriend(final String idFriend) {
        if (idFriend != null) {
            FirebaseDatabase.getInstance().getReference().child("friend").child(StaticConfig.UID)
                    .orderByValue().equalTo(idFriend).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot.getValue() == null) {
                        //email not found
                        dialogWaitDeleting.dismiss();
                        new LovelyInfoDialog(context)
                                .setTopColorRes(R.color.colorAccent)
                                .setTitle("Error")
                                .setMessage("Error occurred during deleting friend")
                                .show();
                    } else {
                        String idRemoval = ((HashMap) dataSnapshot.getValue()).keySet().iterator().next().toString();
                        FirebaseDatabase.getInstance().getReference().child("friend")
                                .child(StaticConfig.UID).child(idRemoval).removeValue()
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        dialogWaitDeleting.dismiss();

                                        new LovelyInfoDialog(context)
                                                .setTopColorRes(R.color.colorAccent)
                                                .setTitle("Success")
                                                .setMessage("Friend deleting successfully")
                                                .show();
                                        Intent intentDeleted = new Intent(FriendsFragment.ACTION_DELETE_FRIEND);
                                        intentDeleted.putExtra("idFriend", idFriend);
                                        context.sendBroadcast(intentDeleted);
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        dialogWaitDeleting.dismiss();
                                        new LovelyInfoDialog(context)
                                                .setTopColorRes(R.color.colorAccent)
                                                .setTitle("Error")
                                                .setMessage("Error occurred during deleting friend")
                                                .show();
                                    }
                                });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            dialogWaitDeleting.dismiss();
            new LovelyInfoDialog(context)
                    .setTopColorRes(R.color.colorPrimary)
                    .setTitle("Error")
                    .setMessage("Error occurred during deleting friend")
                    .show();
        }
    }
     HashMap  tempHashBLockMeForBlockedUser = new HashMap();
    private void unblockFriend(final String idBlockedUser,boolean isBlocked) {
        dialogWaitDeleting.dismiss();


        if(isBlocked) {
            HashMap  reversedHashMap = new HashMap();
            for (Object i : listBlockedID.keySet()) {
                reversedHashMap.put(listBlockedID.get(i), i);
            }

            FirebaseDatabase.getInstance().getReference().child("blcoks")
                    .child(idBlockedUser).child("ME").
                    addListenerForSingleValueEvent(
                            new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.getValue() != null) {
                                        Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                                        while (children.iterator().hasNext()) {
                                            DataSnapshot Keys = children.iterator().next();
                                            tempHashBLockMeForBlockedUser.put(Keys.getValue(),Keys.getKey());
                                        }
                                       notifyDataSetChanged();
                                    }

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });



            final Object ObjectKey = reversedHashMap.get(idBlockedUser);


            if (ObjectKey != null) {

                FirebaseDatabase.getInstance().getReference().child("blcoks")
                        .child(StaticConfig.UID+"/Them").child(ObjectKey.toString()).removeValue()
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                FirebaseDatabase.getInstance().getReference().child("blcoks")
                                        .child(idBlockedUser+"/ME").child(tempHashBLockMeForBlockedUser.get(StaticConfig.UID).toString()).removeValue();
                                listBlockedID.remove(ObjectKey);
                                unblockFriend(idBlockedUser,false);
                                notifyDataSetChanged();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                dialogWaitDeleting.dismiss();
                                new LovelyInfoDialog(context)
                                        .setTopColorRes(R.color.colorAccent)
                                        .setTitle("Error")
                                        .setMessage("Error occurred during unblcoking user")
                                        .show();
                            }
                        });
            } else {
                new LovelyInfoDialog(context)
                        .setTopColorRes(R.color.colorAccent)
                        .setTitle("Error")
                        .setMessage("the user not blocking")
                        .show();
            }
        }else{
            dialogWaitDeleting.dismiss();
            new LovelyInfoDialog(context)
                    .setTopColorRes(R.color.colorAccent)
                    .setTitle("Success")
                    .setMessage("UnBlocking  successfully")
                    .show();
            this.notifyDataSetChanged();
        }
        }





    private void blockFriend(final String idBlockedFriend,boolean isIdBlocked) {
        if(idBlockedFriend != null) {
            if (isIdBlocked) {
                FirebaseDatabase.getInstance().getReference().child("blcoks/"+StaticConfig.UID+"/Them").push().setValue(idBlockedFriend)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    dialogWaitDeleting.dismiss();
                                    dialogWait.dismiss();
                                    FirebaseDatabase.getInstance().getReference().child("blcoks/"+idBlockedFriend+"/ME").push().setValue(StaticConfig.UID);
                                    FirebaseDatabase.getInstance().getReference().child("blcoks/"+StaticConfig.UID+"/Them")
                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    if (dataSnapshot.getValue() != null) {
                                                        Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                                                        while (children.iterator().hasNext()) {
                                                            DataSnapshot Keys = children.iterator().next();
                                                            String mapRecord = (String) Keys.getValue();
                                                            listBlockedID.put(Keys.getKey(),Keys.getValue());
                                                            notifyDataSetChanged();
                                                        }
                                                        blockFriend(idBlockedFriend, false);

                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });

                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                dialogWait.dismiss();
                                new LovelyInfoDialog(context)
                                        .setTopColorRes(R.color.colorAccent)
                                        .setIcon(R.drawable.ic_add_friend)
                                        .setTitle("False")
                                        .setMessage("False to add friend success")
                                        .show();
                            }
                        });
            }else{
                dialogWait.dismiss();
                dialogWaitDeleting.dismiss();
                new LovelyInfoDialog(context)
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(R.drawable.ic_add_friend)
                        .setTitle("Success")
                        .setMessage("Block friend success")
                        .show();
                this.notifyDataSetChanged();

            }
        }
        }

    private void checkBeforBlockFriend(final String idFriend) {
        dialogWait.setCancelable(false)
                .setIcon(R.drawable.ic_add_friend)
                .setTitle("Block friend....")
                .setTopColorRes(R.color.colorPrimary)
                .show();

        //Check xem da ton tai id trong danh sach id chua
        if (listBlockedID.containsValue(idFriend)) {
            dialogWaitDeleting.dismiss();
            dialogWait.dismiss();
            new LovelyInfoDialog(context)
                    .setTopColorRes(R.color.colorPrimary)
                    .setIcon(R.drawable.ic_add_friend)
                    .setTitle("Block")
                    .setMessage("User has been Blocked")
                    .show();
        } else {
            dialogWait.dismiss();
            blockFriend(idFriend, true);
        }
    }





    }



class ItemFriendViewHolder extends RecyclerView.ViewHolder{
    public CircleImageView avata;
    public TextView txtName, txtTime,status_string,status_blocked;
    private Context context;

    ItemFriendViewHolder(Context context, View itemView) {
        super(itemView);
        avata = (CircleImageView) itemView.findViewById(R.id.icon_avata);
        txtName = (TextView) itemView.findViewById(R.id.txtName);
        txtTime = (TextView) itemView.findViewById(R.id.txtTime);
        status_string = (TextView) itemView.findViewById(R.id.status_string);
        status_blocked = (TextView)itemView.findViewById(R.id.status_blocked);
        this.context = context;
    }
}

